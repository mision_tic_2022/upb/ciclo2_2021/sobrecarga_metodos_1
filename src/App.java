public class App {
    public static void main(String[] args) throws Exception {
        
        Reloj objReloj_1 = new Reloj();
        objReloj_1.setHora(17);
        Reloj objReloj_2 = new Reloj(16, 52);
        Reloj objReloj_3 = new Reloj(16, 52, 40);

        System.out.println("Reloj 1: \n"+objReloj_1);
        System.out.println("Reloj 2: \n"+objReloj_2);
        System.out.println("Reloj 3: \n"+objReloj_3);


    }
}
