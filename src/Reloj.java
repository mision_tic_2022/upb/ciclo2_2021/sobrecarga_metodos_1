public class Reloj{
    /*************
     * Atributos
     ************/
    private int hora;
    private int minuto;
    private int segundo;

    /****************
     * Constructores
     ***************/
    public Reloj(){
        this.hora = 0;
        this.minuto = 0;
        this.segundo = 0;
    }

    public Reloj(int hora, int minuto){
        this.hora = hora;
        this.minuto = minuto;
    }

    public Reloj(int hora, int minuto, int segundo){
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    

    //Getters and Setters

    public String toString() {
        return "Reloj [hora=" + hora + ", minuto=" + minuto + ", segundo=" + segundo + "]";
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }

     
    

}